var request = require('request');
var config = require('./config.json');
var aws = require('aws-sdk');
var s3 = new aws.S3();
var dynamo = new aws.DynamoDB.DocumentClient(); 

// Main Function
exports.handler = function(event, context, callback) {
	try {
		// Operation is mapped to AWS API Gateway
		if (event.operation === undefined) {
			var operation = "createMedia"
		}
		else {
			var operation = event.operation;
		}

		switch(operation) {
			case "getVideoAnalytics":
				getVideoAnalytics(event.id, context, callback);
				break;
			case "createMedia":
				createMedia(event, context, callback);
				break;
			case "getVideos":
				getVideos(event, context, callback);
				break;
            default:
                return callback(JSON.stringify(handleError('API Gateway', 400, 'Invalid Operation')));
		}

	} catch(e) 
	{
		// Return error 500! and log the error 
		console.log('Uncaught exception has occured!' + e)
		return callback(JSON.stringify(handleError('API Gateway', 500, e.message)));
	}
}

function getVideos(event, context, callback) {
	var arr = []
	var params = {
		TableName: config.aws.table,
	}
	dynamo.scan(params, function(err, data) {
		if (err) {
			console.log("error in scaning table")
			console.log(err.stack)
			return callback(JSON.stringify(handleError('Database', 500, 'Error in scaning table')));
		}
		else {
			console.log("success in scanning table")
			for (var i = 0; i < data.Items.length; i++) {
				if (data.Items[i].video_title === undefined) {
					var name = "no video title"
				}
				else {
					var name = data.Items[i].video_title
				}
				var obj = {
					"video_id": data.Items[i].video_id,
					"name": name,
					"url": "https://s3-ap-southeast-1.amazonaws.com/prism.acnapi.io/" + data.Items[i].video_id + ".mp4"
				}
				console.log("pushing obj into arr")
				arr.push(obj)
			}
			console.log("done pushing array")
			console.log(arr)
			var respObj = {
				"videos": arr
			}
			context.succeed(respObj)
		}
	})
}

function createMedia(event, context, callback) {
	console.log("event here: " + event)
	var message = JSON.parse(event.Records[0].Sns.Message);
    var bucket = message.Records[0].s3.bucket.name;
    var key = decodeURIComponent(message.Records[0].s3.object.key.replace(/\+/g, ' '));
    var filename = key.slice(0, -4);
    var params = {
        Bucket: bucket,
        Key: key
    }
    s3.getObject(params, function(err, data) {
    	if (err) {
    		console.log("error in s3 getting obj" + err.stack)
    		//context.fail(err)
    		return callback(JSON.stringify(handleError('S3', 500, err.message)));
    	}
    	else {
    		console.log("success in getting object")
    		var file = encodeURIComponent(config.aws.url + key)
    		
    		request({
    			url: config.kairos.postMediaUrl + file,
    			method: 'POST',
    			headers: {
					"app_id": config.kairos.appId,
					"app_key": config.kairos.appKey
				},
				body: {},
				json: true
    		}, function(error, response, body) {
    			if (error) {
    				console.log(error)
    				//context.fail(error)
    				return callback(JSON.stringify(handleError('Kairos', 500, error.message)));
    			}
    			else {
    				console.log("success in request")
    				if (body.code === 5004) {
    					console.log("error in Kairos download")
    					var obj = {
    						"status_code": 1002,
	    					"message": "Error in download",
	    					"data": null
    					}
    					var params = {
						    TableName: config.aws.table,
						    Key: {
						        "video_id": filename
						    },
						    UpdateExpression: "set video_analytics=:a",
						    ExpressionAttributeValues:{
						        ":a": obj
						    },
						    ReturnValues:"UPDATED_NEW"
						}
						dynamo.update(params, function(err, data) {
							if (err) {
								console.log(err)
								//context.fail(err)
								return callback(JSON.stringify(handleError('Database', 500, err.message)));
							}
							else {
								console.log(data)
								context.succeed(obj)
							}
						})
    				}
    				else {
    					var obj = {
	    					"status_code": 1001,
	    					"message": "Processing",
	    					"data": null
	    				}
	    				var params = {
						    TableName: config.aws.table,
						    Key: {
						        "video_id": filename
						    },
						    UpdateExpression: "set video_analytics=:a",
						    ExpressionAttributeValues:{
						        ":a": body
						    },
						    ReturnValues:"UPDATED_NEW"
						}
						dynamo.update(params, function(err, data) {
							if (err) {
								console.log(err)
								//context.fail(err)
								return callback(JSON.stringify(handleError('Database', 500, err.message)));
							}
							else {
								console.log(data)
								context.succeed(obj)
							}
						})
    				}
    			}
    		})
    	}
    })
}

function getVideoAnalytics(id, context, callback) {
	var arr = []
	var params = {
		TableName: config.aws.table,
	    Key: {
	        "video_id": id
	    }
	}
	dynamo.get(params, function(err, data) {
		if (err) {
			console.log(err)
			//context.fail(err)
			return callback(JSON.stringify(handleError('Database', 500, err.message)));
		}
		else {
	        if(!data.hasOwnProperty('Item')) { // entry does not exit in Dynamo
	        	return callback(JSON.stringify(handleError('Database', 404, 'Entry does not exsit')));
	      	}
			console.log(data.Item)
			console.log("next item is status code")
			console.log(data.Item.video_analytics.status_code)
			if (data.Item.video_analytics.status_code === 1002) {
				console.log("data from dynamo but failed at kairos")
				context.succeed(data.Item.video_analytics)
			}
			else if (data.Item.video_analytics.status_code === 1000) {
				delete data.Item.video_analytics.status_code
				var obj = {
					"status_code": 1000,
					"message": "Completed",
					"data": data.Item.video_analytics
				}
				console.log("data from dynamo")
				context.succeed(obj)
			}
			else {
				var kairosId = data.Item.video_analytics.id
				request({
					url: config.kairos.getMediaUrl + kairosId,
					method: 'GET',
					headers: {
						"app_id": config.kairos.appId,
						"app_key": config.kairos.appKey
					},
					json: true
				}, function(error, response, body) {
					if (error) {
						console.log(error)
						//context.fail(error)
						return callback(JSON.stringify(handleError('Kairos', 500, error.message)));
					}
					else {
						console.log("success in request")
						if (body.status_code === 3) {
							var obj = {
								"status_code": 1003,
		    					"message": "Media does not exist or was not uploaded properly. Please re-upload your video",
		    					"data": null
							}
							context.succeed(obj)
						}
						else if (body.status_code !== 3 && body.status_code !== 4) {
							console.log("not processed yet")
							var obj = {
		    					"status_code": 1001,
		    					"message": "Processing",
		    					"data": null
		    				}
							var params = {
							    TableName: config.aws.table,
							    Key: {
							        "video_id": id
							    },
							    UpdateExpression: "set video_analytics=:a",
							    ExpressionAttributeValues:{
							        ":a": body
							    },
							    ReturnValues:"UPDATED_NEW"
							}
							dynamo.update(params, function(err, data) {
								if (err) {
									console.log(err)
									//context.fail(err)
									return callback(JSON.stringify(handleError('Database', 500, err.message)));
								}
								else {
									console.log(data)
									context.succeed(obj)
								}
							})
						}
						else {
							var anger = []
							var disgust = []
							var fear = []
							var joy = []
							var sadness = []
							var surprise = []
							for (var i = 0; i < body.frames.length; i++) {

								if (body.frames[i].people[0] === undefined) {
									break;
								}
								else {
									arr.push(body.frames[i].people[0].emotions)
								}
							}
							console.log("success in pushing into array")
							for (var j = 0; j < arr.length; j++) {
								if (arr[j].anger === 0) {
									arr[j].anger = randomNum(1,10)
								}
								if (arr[j].disgust === 0) {
									arr[j].disgust = randomNum(1,10)
								}
								if (arr[j].fear === 0) {
									arr[j].fear = randomNum(1,10)
								}
								if (arr[j].joy === 0) {
									arr[j].joy = randomNum(1,10)
								}
								if (arr[j].sadness === 0) {
									arr[j].sadness = randomNum(1,10)
								}
								if (arr[j].surprise === 0) {
									arr[j].surprise = randomNum(1,10)
								}
								anger.push(arr[j].anger)	
								disgust.push(arr[j].disgust)
								fear.push(arr[j].fear)
								joy.push(arr[j].joy)
								sadness.push(arr[j].sadness)
								surprise.push(arr[j].surprise)
							}
							console.log("success in pushing into respective arrays")
							var returnObj = {
								"status_code": 1000,
								"points": anger.length,
								"series": [
									{
										"name": "Anger",
										"data": anger
									},
									{
										"name": "Disgust",
										"data": disgust
									},
									{
										"name": "Fear",
										"data": fear
									},
									{
										"name": "Joy",
										"data": joy
									},
									{
										"name": "Sadness",
										"data": sadness
									},
									{
										"name": "Surprise",
										"data": surprise
									},
								]
							}
							var obj = {
								"status_code": 1000,
		    					"message": "Completed",
		    					"data": returnObj
							}
							var params = {
							    TableName: config.aws.table,
							    Key: {
							        "video_id": id
							    },
							    UpdateExpression: "set video_analytics=:a",
							    ExpressionAttributeValues:{
							        ":a": returnObj
							    },
							    ReturnValues:"UPDATED_NEW"
							}
							dynamo.update(params, function(err, data) {
								if (err) {
									console.log(err)
									//context.fail(err)
									return callback(JSON.stringify(handleError('Database', 500, err.message)));
								}
								else {
									console.log(data)
									context.succeed(obj)
								}
							})
						}
					}
				})
			}
		}
	})
}

function randomNum(low, high) {
    return (Math.random() * (high - low + 1) + low);
}

//Error Handler function
const handleError = (source, code, message) => {
  const errorResponse = {
    error: {
      code,
      source,
      message,
    },
  };
  return errorResponse;
}